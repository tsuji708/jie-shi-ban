<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>簡易Twitter</title>
    </head>
    <body>
        <div class="header">
    		<c:if test="${ empty loginUser }">
        		<a href="login">ログイン</a>
        		<a href="signup">登録する</a>
    		</c:if>
    		<c:if test="${ not empty loginUser }">
        		<a href="./">ホーム</a>
        		<a href="settings">設定</a>
        		<a href="logout">ログアウト</a>

        <form action="newMessage" method="post">
        	タイトル<br />
        	<textarea name="title" cols="30" rows="1" class="tweet-box"></textarea><br />
            本文（1000字まで）<br />
            <textarea name="message" cols="100" rows="10" class="tweet-box"></textarea><br />
            カテゴリ<br />
            <textarea name="category" cols="10" rows="1" class="tweet-box"></textarea>
            <br />
            <input type="submit" value="投稿">
        </form>
    		</c:if>
		</div>

    </body>
</html>

